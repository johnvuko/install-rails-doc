# Création de l'instance AWS EC2

- aller sur https://eu-west-1.console.aws.amazon.com/ec2/v2/home?region=eu-west-1#LaunchInstanceWizard
- choisir la distribution Ubuntu (actuellement Ubuntu Server 16.04 LTS)
- choisissez "create a new key pair"
- récupérer le certificat et le garder de façon sécurisé et fiable, sans ce fichier rien n'est possible

# Installation du serveur

## Connexion au serveur

- aller sur https://eu-west-1.console.aws.amazon.com/ec2/v2/home?region=eu-west-1#Instances:tag:Name=MailAlert;sort=instanceId
- cliquer sur "connect", 2 commandes sont indiquées:
	- `chmod 400 ec2-key.pem` qui restreint les droits sur ce fichier, sinon ssh génère une erreur.
	- `ssh -i "ec2-key.pem" ubuntu@ec2-54-246-174-81.eu-west-1.compute.amazonaws.com`
	- `ec2-key.pem` correspond au certificat généré lors de l'installation.
	- `ec2-54-246-174-81.eu-west-1.compute.amazonaws.com` est l'adresse du serveur, disponible depuis la liste des instances.

Pour faire plus simple, enregistrez ce certificat dans `~/.ssh/example.com.pem`, `example.com` étant le nom de domaine du serveur.

```bash
echo "
Host example
    User ubuntu
    Hostname ec2-54-246-174-81.eu-west-1.compute.amazonaws.com
    IdentityFile ~/.ssh/example.com.pem
" >> ~/.ssh/config
chmod 400 ~/.ssh/example.com.pem
```

Il est maintenant possible de se connecter au serveur juste en faisant `ssh example`.

## Installation

### Mise à jour de la distribution

Les versions installées par défaut sont rarement à jour.

```bash
sudo apt-get -y update
sudo apt-get -y upgrade
```

Si il y a des messages de configuration de mise à jour (par exemple grub qui demande les actions sur les anciens fichiers) alors laisser les choix par défaut.

### Installation des services de bases

```bash
sudo apt-get install -y sudo build-essential logrotate htop screen openssl git curl jq ntpdate imagemagick ghostscript xpdf libxslt-dev libxml2-dev nodejs
```

- libxslt-dev et libxml2-dev sont requis pour Ruby, pas mal de dépendances en ont besoin.
- imagemagick, ghostscript et xpdf sont utile pour le traitement d'images et de pdf.
- nodejs est utilisé par des libs comme moteur Javascript.

### Installation de ntpdate

ntpdate sert à garder l'heure du serveur à jour.
Il arrive que le serveur se désynchronise quand il subit une charge élevé ce qui peut poser des problèmes avec certaines systèmes de sécurité.

```bash
echo '#!/bin/sh

test -x /usr/sbin/ntpdate-debian || exit 0
/usr/sbin/ntpdate-debian' | sudo tee /etc/cron.hourly/ntpdate
sudo chmod +x /etc/cron.hourly/ntpdate
```

### Génération clef SSH

Génération d'une clef RSA de 4096 bits sans passphrase.

```bash
ssh-keygen -t rsa -b 4096 -N ""
```

### Installation de PostgreSQL

```bash
sudo apt-get install -y postgresql libpq-dev postgresql-contrib
```

Creation de l'utilisateur de la database, nommé ici `db` avec le mot de passe `aY1v2RA5mZ`

```bash
sudo su postgres -c "psql"
CREATE USER db WITH CREATEDB LOGIN ENCRYPTED PASSWORD 'aY1v2RA5mZ';

echo "
local   all             db                                      peer
host    all             db              127.0.0.1/32            ident
host    all             db              ::1/128                 ident" | sudo tee -a /etc/postgresql/9.5/main/pg_hba.conf
sudo service postgresql restart
```

### Installation de RVM

RVM permet d'isoler la version de Ruby ainsi que ses gems par projets.
Cela permet d'avoir plusieurs projets sur un serveur sans que les gems n'entrent en conflit.


```bash
curl -L https://get.rvm.io | bash
source /home/ubuntu/.rvm/scripts/rvm
```

### Installation de nginx

La configuration fournit utilise l'utilisateur `ubuntu` et le groupe `ubuntu` pour lancer nginx.
Les dossiers des projets doivent avoir les droits en lecture pour cet utilisateur (par défaut le cas).
Le fichier nginx.tar contient une configuration par défaut pour nginx.

Depuis votre ordinateur:

Si vous aviez bien configuré `.ssh/config` faites:

```bash
scp nginx.tar example:/home/ubuntu/nginx.tar
```

sinon

```bash
scp -i ec2-key.pem nginx.tar ubuntu@ec2-54-246-174-81.eu-west-1.compute.amazonaws.com:/home/ubuntu/nginx.tar
```

Sur le serveur:

```bash
sudo apt-get install -y nginx
sudo cp /etc/nginx/nginx.conf /etc/nginx/nginx.conf.default
sudo rm /etc/nginx/sites-enabled/default
sudo mkdir /etc/nginx/certs/
sudo tar -xpf ~/nginx.tar -C /etc/nginx/
sudo chown -R root:root /etc/nginx/
rm ~/nginx.tar
sudo openssl dhparam 4096 -out /etc/nginx/certs/dhparam.pem
```

### Ajout d'un site dans nginx

Un exemple de configuration est disponible dans le dossier `/etc/nginx/sites-available/example.com`.

Pour activer un site avec nginx:

```bash
sudo ln -s /etc/nginx/sites-available/example.com /etc/nginx/sites-enabled/example.com
sudo service nginx restart
```

Pour désactiver un site avec nginx:

```bash
sudo rm /etc/nginx/sites-enabled/example.com
sudo service nginx restart
```

### Ajout de logrotate pour un site

Créer un fichier `/etc/logrotate.d/example` avec pour contenu:

```
/srv/example-production/shared/log/*.log {
  daily
  missingok
  rotate 7
  compress
  delaycompress
  notifempty
  copytruncate
}
```

## Autoriser les ports HTTP et HTTPS sur les instances Amazon EC2

### Création du groupe de sécurité

Il suffit de créer un groupe de sécurité "Security Groups", autorisant le traffic entrant sur le port 80 et 443.
Cette manipulation n'est requise qu'une seule fois.
Pour cela allez sur https://eu-west-1.console.aws.amazon.com/ec2/v2/home?region=eu-west-1#SecurityGroups:sort=groupId et remplissez le formulaire tel que dans

![SecurityGroup](security_group.png)

### Ajout du groupe de sécurité à l'instance EC2

- Allez sur la page de gestion des instances https://eu-west-1.console.aws.amazon.com/ec2/v2/home?region=eu-west-1#Instances:sort=instanceId
- Cliquez sur Actions / Networking / Change Security Groups
- cocher le groupe "HTTP and HTTPS"
