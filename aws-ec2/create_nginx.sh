#!/bin/sh

set -e

cd nginx/
tar -cpf ../nginx.tar --exclude='.DS_Store' *
echo "Done"