upstream example{
        server 127.0.0.1:3000;
        # or
        server unix:/srv/example/tmp/sockets/puma.sock;
}

# Force www and HTTPS
server{
        include /etc/nginx/snippets/listen;
        server_name example.com www.example.com;

        include /etc/nginx/snippets/well_known;

        return 301 https://www.example.com$request_uri;
}

# Force www
server{
        include /etc/nginx/snippets/listen_ssl;

        ssl_certificate /etc/nginx/certs/example.crt;
        ssl_certificate_key /etc/nginx/certs/example.key;
        ssl_trusted_certificate /etc/nginx/certs/example.chain.crt;
        
        server_name example.com;
        return 301 https://www.example.com$request_uri;
}

server {
        include /etc/nginx/snippets/listen_ssl;

        ssl_certificate /etc/nginx/certs/example.crt;
        ssl_certificate_key /etc/nginx/certs/example.key;
        ssl_trusted_certificate /etc/nginx/certs/example.chain.crt;

        server_name www.example.com;
        root /srv/example/public;

        include /etc/nginx/snippets/rails;

        location @ruby {
                include /etc/nginx/snippets/rails_proxy;

                proxy_pass http://example;
        }
}
