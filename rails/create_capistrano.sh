#!/bin/sh

set -e

zip -r capistrano.zip capistrano/ -x "*.DS_Store"
