lock "3.9.1"

set :application, "example"
set :repo_url, "git@bitbucket.org:eviljojo22/mom-example.git"

set :deploy_to, -> { "/srv/#{fetch(:application)}-#{fetch(:stage)}" }

 set :ssh_options, {
  keys: [
    File.join(ENV["HOME"], ".ssh", "id_rsa"),
    File.join(ENV["HOME"], ".ssh", "id_dsa"),
    File.join(ENV["HOME"], ".ssh", "example.com.pem")
  ],
  forward_agent: true
}

append :linked_files, "config/database.yml", ".env"
append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system"

set :rack_env, fetch(:stage)
set :rails_env, fetch(:stage)

set :default_env, { 
  'RACK_ENV' => fetch(:stage),
  'RAILS_ENV' => fetch(:stage)
}

ruby_version = File.read(File.join(Dir.pwd, '.ruby-version')).strip
ruby_gemset = File.read(File.join(Dir.pwd, '.ruby-gemset')).strip

set :rvm_ruby_version, "#{ruby_version}@#{ruby_gemset} --create"
set :rvm_type, :user
