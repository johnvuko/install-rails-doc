namespace :puma do
  desc 'Upload Puma config file'
  task :upload do
    on roles(fetch(:puma_role)) do |role|
      upload! File.join(Dir.pwd, '/config/puma.rb'), File.join(shared_path, '/puma.rb')
    end
  end
end