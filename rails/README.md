# Création d'un nouveau projet Ruby On Rails

## Base

```bash
mkdir example
cd example

echo "2.4.2" > .ruby-version
echo "example" > .ruby-gemset

echo "Reload RVM"
cd ..
cd example

gem install rails
rails new ../example -d postgresql --skip-turbolinks --skip-action-cable --skip-spring

cp config/database.yml config/database.yml.example
echo "
.DS_Store
.env
config/database.yml
public/assets
public/system" >> .gitignore

echo "gem 'dotenv-rails'" >> Gemfile

echo "state_path 'tmp/pids/puma.state'
bind 'unix://tmp/sockets/puma.sock'" >> config/puma.rb
```

- `--skip-turbolinks` désactive [turbolinks](https://github.com/turbolinks/turbolinks) qui évite le rechargement complet des pages web mais demande des modifications du code Javascript.
- `--skip-action-cable` désactive [ActionCable](http://edgeguides.rubyonrails.org/action_cable_overview.html) qui gère les WebSockets.
- `--skip-spring` désactive spring qui peut poser problème car l'environment est mal rechargé.
- `config/database.yml` est copié dans `config/database.yml.example` (ne pas renommer example ici) car on a tous une configuration différente et elle ne peut donc pas être dans le dépot.

Dans le `Gemfile`, décommenter la ligne `gem 'therubyracer', platforms: :ruby`, puis faire un `bundle install`.
Il est nécessaire d'avoir `nodejs` d'installé sur les serveurs Ubuntu.


## Création d'un nouvel environnement staging

Par défaut, il n'existe que l'environnement development, test et production.
Il suffit de:
- rajouter `staging` dans `config/database.yml`
- rajouter `staging` dans `config/secrets.yml`
- copier le fichier `config/environments/production.rb` dans `config/environments/staging.rb`

## Configuration Ruby On Rails

Penser à modifier le `config/database.yml`, puis créer la base de donnée avec:

```bash
bundle exec rake db:create
```

## Configuration de Capistrano

[Capistrano](https://github.com/capistrano/rails) permet de déployer un projet sur un serveur distant.

Ajoutez Capistrano au Gemfile:

```ruby
group :development do
  gem 'capistrano', '~> 3.6'
  gem 'capistrano-rails', '~> 1.3'
  gem 'capistrano-rvm'
  gem 'capistrano3-puma'
end
```

Mettez à jour vos gems:

```bash
bundle install
```

- copiez les fichiers de configuration de Capistrano `capistrano.zip` et les mettre à la racine du projet.
- modifiez les varaibles `application`, `repo_url` et le chemin du certificat `example.com.pem` dans le fichier `config/deploy.rb`
- modifiez le `server` dans `config/deploy/staging.rb` et `config/deploy/production.rb`

# Deploiement d'un nouveau projet / environnement

Ici on va déployer en production.

Sur le serveur:

```bash
sudo mkdir /srv/example-production
sudo chown ubuntu:ubuntu /srv/example-production
mkdir -p /srv/example-production/shared/config/
createdb -U db -W -h 127.0.0.1 example_production
```

Créer votre fichier `/srv/exmaple-production/shared/config/database.yml`.
Créer votre fichier `/srv/exmaple-production/shared/.env` qui doit contenir `SECRET_KEY_BASE`, généré sur votre machine avec la commande `rake secret` ou `rails secret`.


Sur votre ordinateur, lancer un premier déploiement:

```bash
cap production puma:upload
cap production deploy
```

Penser ensuite à créer la configuration nginx à partir de `/etc/nginx/sites-available/example.com`.

## Problèmes connus

Si vous avez un message du type `Required ruby-2.4.2 is not installed` alors faites sur le serveur:

```bash
rvm install ruby-2.4.2
rvm use ruby-2.4.2@global do gem install bundler
```


Si vous avez un problème de permissions SSH `Permission denied`, faites sur votre ordinateur:

```bash
eval `ssh-agent`
ssh-add
```
