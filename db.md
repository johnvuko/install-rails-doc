# Création d'un dump de la base de donnée

 - se connecter en SSH sur le serveur `ssh moexpo@vm-prod-www.moexpo.safidomain.com`
 - aller dans le dossier du projet `cd /var/www/mo-extranet-v2_production/current`

Executer la commande:

    $ pg_dump -Fc --no-acl --no-owner --user USER -h HOST -W DB_NAME > /home/moexpo/db.dump

Les valeurs `USER`, `HOST` et `DB_NAME` sont visible dans le fichier `config/database.yml`.
La commande finale ressemblera à:

    $ pg_dump -Fc --no-acl --no-owner --user eplan -h vm-prod-bdd.moexpo.safidomain.com -W eplan > /home/moexpo/db.dump

Il faut maintenant copier ce fichier sur votre machine:

    $ scp moexpo@vm-prod-www.moexpo.safidomain.com:/home/moexpo/db.dump ./db.dump

# Restaurer un dump de la base de donnée sur sa mahcine locale

- aller dans le dossier du projet
- effacer l'ancienne base de donnée `RAILS_ENV=development bundle exec rake db:drop`
- créer une nouvelle base de donnée `RAILS_ENV=development bundle exec rake db:create`

Restaurer le dump avec la commande:

    $ pg_restore --verbose --user USER -h HOST --clean --no-acl --no-owner -d DB_NAME /home/moexpo/db.dump

Les valeurs `USER`, `HOST` et `DB_NAME` sont visible dans le fichier `config/database.yml`.
La commande finale ressemblera à:

    $ pg_restore --verbose --user postgres -h localhost --clean --no-acl --no-owner -d maison_objet_dev db.dump

Il faut ensuite mettre à jour le fichier `db/schema.rb` sans quoi rails ne connait pas la structure de la base de donnée actuelle:

    $ RAILS_ENV=development bundle exec rake db:schema:dump

Si il y en a, il faut éxecuter les migrations en attentes:

    $ RAILS_ENV=development bundle exec rake db:migrate

# Restaurer un dump de la base de donnée sur une mahcine distante (integration / staging)

Il faut stopper les services nginx et apache afin d'éviter toute connexion à la base de donnée qui empecherait donc sa modification.

Pour arreter les services:

    $ sudo service apache2 stop
    $ sudo service nginx stop

Il faut ensuite couper toutes les connections actives sur la base de donnée:

    $ RAILS_ENV=integration bundle exec rails db

```sql
SELECT pg_terminate_backend(pg_stat_activity.pid)
FROM pg_stat_activity
WHERE pg_stat_activity.datname = 'DB_NAME'
AND pid <> pg_backend_pid();
```

Remplacer `DB_NAME` par le nom de la base de donnée.

Maintenant vous pouvez effectuer les mêmes commandes sur la machine distante que pour "restaurer un dump de la base de donnée sur sa mahcine locale".

Pour relancer les services:

    $ sudo service apache2 start
    $ sudo service nginx start
